# Feeder Monitor

A blood temperature maintainter and logger.

#### logviewer
Designed with Google Polymer webcomponents, which must first be installed for the page to render correctly.

Bower is used for package management.
 - To install necessary components, defined in ```bower.json```, first install bower with ```npm install bower -g``` the ```-g``` installs bower globally. -  - Next, working in  ```logviewer/```, run ```bower update```, this will download the webcomponents in ```bower_components/```.
  -  ```index.html``` is now ready to be served.
   
   Database address and credentials to be edited as needed. Database schema located in ```SQL/```


#### firmware
   ```feedermonitor.ino``` runs on the ESP8266. There is a boards addon in recent arduino versions that makes this a matter of copying in a URL. This then installs all files necessary for cross compilation onto the ESP8266.
    - The host address that served up the web logviewer interface and hosted the database will need to bed edited along with all subsequent references to the page on that domain.
     
      
### hardware
The PCB is minimal, with FETs to control the resistive heating pads and one wire temperature sensors for thermal measurements. 
      - Sensors were erroneously put on separate pins, defeating the fact that multiple can be discovered on one hardware pin. 
      - From testing, 5v is not enough to get the heating pads to a suitable temperature. 9v works well, but since Vcc for the temp sensors is on the same bus as the heating pads, a regaulator needs to be added.