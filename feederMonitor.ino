#include <OneWire.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#define NUM_SENSORS 3

#define PAD1_PIN  16
#define PAD2_PIN  12
#define PAD3_PIN  13
#define PAD4_PIN  14

byte pads[3] = {PAD2_PIN, PAD3_PIN, PAD4_PIN};

//const char *ssid = "WiseGuy";
//const char *password = "silver+/-10%";

//const char *ssid = "WIN_3b28";
//const char *password = "cc77cc4b";

const char *ssid = "TigerMosq";
const char *password = "ZaP2015WC";

long then = 0;

long interval = 10000;
float thresh = 98.6;

OneWire  ts1(2);
OneWire  ts2(5);
OneWire  ts3(4);

OneWire sensors[3] = {ts1, ts2, ts3};

byte i;

byte addrs[NUM_SENSORS][8];
byte data[NUM_SENSORS][12];

byte type_s;
byte present = 0;
float celsius[NUM_SENSORS], fahrenheit[NUM_SENSORS];

const char dataHost[] = "ruffner.noip.me";

IPAddress ip;
String ipStr;
String url;

int runNumber = -1;

bool first_post = 1;

void setup() {
  // setup pad pins as outputs, initially low
  int i;
  for ( i=0; i<NUM_SENSORS; i++ ){
    pinMode(pads[i], OUTPUT);
    digitalWrite(pads[i], LOW);
  }
  
  Serial.begin(115200);
  WiFi.begin ( ssid, password );
  
  delay(100);
  Serial.println("Starting temp monitoring...");
  Serial.print("Connecting to WiFi");

  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  // print SSID and IP address
  Serial.println ( "" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );
  ip = WiFi.localIP();
  ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
  
  // search busses for one wire temp sensors
  setTempAddrs();

  delay(500);

  // get associated run ID to post with gathered data
  getRunNumber();

  // get current threshold from database
  getThreshold();

  // get update rate from database
  getInterval();
}

void loop() {
  // update the temperature every 200ms
  int j;
  if(millis() - then > interval) {
    url = "/feedermonitor/logviewer/src/log.php?values=";
    
    for ( j=0; j<NUM_SENSORS; j++ ){
      sensors[j].reset();
      sensors[j].select(addrs[j]);
      sensors[j].write(0x44, 1);        // start conversion, with parasite power on at the end
      
      delay(750);     // maybe 750ms is enough, maybe not
      // we might do a ts.depower() here, but the reset will take care of it.
      
      present = sensors[j].reset();
      sensors[j].select(addrs[j]);    
      sensors[j].write(0xBE);         // Read Scratchpad
      for ( i = 0; i < 9; i++) {           // we need 9 bytes
        data[j][i] = sensors[j].read();
      }
    
      int16_t raw = (data[j][1] << 8) | data[j][0];
      if (type_s) {
        raw = raw << 3; // 9 bit resolution default
        if (data[j][7] == 0x10) {
          // "count remain" gives full 12 bit resolution
          raw = (raw & 0xFFF0) + 12 - data[j][6];
        }
      } else {
        byte cfg = (data[j][4] & 0x60);
        // at lower res, the low bits are undefined, so let's zero them
        if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
        else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
        else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
        //// default is 12 bit resolution, 750 ms conversion time
      }
      celsius[j] = (float)raw / 16.0;
      fahrenheit[j] = celsius[j] * 1.8 + 32.0;
      Serial.print("t " + String(j+1) + ": ");
      Serial.println(fahrenheit[j]);
      //Serial.println(" F");
    
      processPad(fahrenheit[j], j); 
      then = millis();
    
      url += String(fahrenheit[j]);
      if ( j<NUM_SENSORS-1 ) 
        url += ",";
    } 
    url += "&runNumber=";
    url += String(runNumber);

    if(first_post) {
      url += "&new";
      first_post = 0;
    }

    Serial.print("Requesting URL: ");
    Serial.println(url);

    WiFiClient client;
    const int httpPort = 80;
    if (!client.connect(dataHost, httpPort)) {
      Serial.println("connection failed");
      return;
    }

    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + ipStr + "\r\n" + 
               "Connection: close\r\n\r\n");

    delay(20);
    
    // Read all the lines of the reply from server and print them to Serial
    while(client.available()){
      String line = client.readStringUntil('\r');
      //Serial.print(line);
    }
  }
}

void getInterval() {
  // get the threshold temp value from server
  
  url = "/feedermonitor/logviewer/src/log.php?getInterval";
  Serial.println("Getting threshold temperature from server...");
  WiFiClient client;
  
  const int httpPort = 80;
  if (!client.connect(dataHost, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + ipStr + "\r\n" + 
               "Connection: close\r\n\r\n");
  
  delay(40);
  
  // Read all the lines of the reply from server and print them to Serial
  String line;
  while(client.available()){
    line = client.readStringUntil('\r');
  }
  Serial.print("got an update rate of ");
  interval = 1000*line.toInt();
  Serial.print(interval);
  Serial.println(" from server");
}

void getThreshold() {
  // get the threshold temp value from server
  
  url = "/feedermonitor/logviewer/src/log.php?getThresh";
  Serial.println("Getting threshold temperature from server...");
  WiFiClient client;
  
  const int httpPort = 80;
  if (!client.connect(dataHost, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + ipStr + "\r\n" + 
               "Connection: close\r\n\r\n");
  
  delay(40);
  
  // Read all the lines of the reply from server and print them to Serial
  String line;
  while(client.available()){
    line = client.readStringUntil('\r');
    //Serial.print(line);
  }
  Serial.print("got threshold of ");
  thresh = line.toFloat();
  Serial.print(thresh);
  Serial.println(" from server");
}

void getRunNumber() {
  // get this log runs id number from the server
  url = "/feedermonitor/logviewer/src/log.php?nextId";
  Serial.println("Getting next run number from server...");
  
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(dataHost, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  delay(10);
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + ipStr + "\r\n" + 
               "Connection: close\r\n\r\n");
  
  delay(40);
  
  // Read all the lines of the reply from server and print them to Serial
  String line;
  while(client.available()){
    line = client.readStringUntil('\r');
    //Serial.print(line);
  }
  runNumber = line.toInt();
  Serial.print("got run number of ");
  Serial.print(runNumber);
  Serial.println(" from server"); 
}

void setTempAddrs() {
  // look for and connect to DS18B20 temp sensor
  Serial.println("\nLooking for sensors..");
  int j;
  for( j=0; j<NUM_SENSORS; j++ ){
    if(!sensors[j].search(addrs[j])) {
      Serial.println("Couldn't find temperature sensor");  
    } else {
      Serial.print("Found device with address:");
      for( i = 0; i < 8; i++) {
        Serial.write(' ');
        Serial.print(addrs[j][i], HEX);
      }
      Serial.print("Checking CRC..");
    
      if (OneWire::crc8(addrs[j], 7) != addrs[j][7]) {
        Serial.println("CRC is not valid!");
        return;
      } else {
        Serial.println("CRC passed!");
      }
    }
  }
  

  
}

void processPad(float fTemp, int padNum) {
  if ( fTemp < thresh && !digitalRead(pads[padNum])) {
    digitalWrite(pads[padNum], HIGH);
    Serial.println("Turned pad on");
  } else if ( fTemp > thresh && digitalRead(pads[padNum])) {
    digitalWrite(pads[padNum], LOW);
    Serial.println("Turned pad off");
  }
}
